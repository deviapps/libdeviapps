<?php
/**
 * @author		Cyprian Sniegota
 * @package		devilib.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

/**
 * General Controller
 */
class DevilibController extends JController {
	function display($cachable = false) {
		// set default view if not set
		JRequest::setVar('view', JRequest::getCmd('view', 'main'));
		$view = JRequest::getCmd('view', 'main');
		parent::display($cachable);
		// add Sub menu
		DevilibHelper::addSubmenu($view);
	}
}