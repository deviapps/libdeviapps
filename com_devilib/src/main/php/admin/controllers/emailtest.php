<?php
/**
 * @author		Cyprian Sniegota
 * @package		devilib.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class DevilibControllerEmailtest extends JController {
	public function testme() {
		echo "testing";
		$model = $this->getModel();
		$model->testEmail();
		exit;
	}
	
	public function getModel($name = 'Emailtest', $prefix = 'DevilibModel') {
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
}