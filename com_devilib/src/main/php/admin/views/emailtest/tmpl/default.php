<?php
/**
 * @author		Cyprian Sniegota
 * @package		devilib.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted Access');
JHtml::_('behavior.tooltip');
?>
<form action="<?php echo JRoute::_('index.php?option=com_devilib&view=sets'); ?>" method="post" name="adminForm">
	<input type="submit" name="" value="Testuj" />
	<div>
		<input type="hidden" name="task" value="emailtest.testme" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>