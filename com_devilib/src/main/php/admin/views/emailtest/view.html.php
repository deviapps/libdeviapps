<?php
/**
 * @author		Cyprian Sniegota
 * @package		devilib.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

/**
 * Sets view
 */
class DevilibViewEmailtest extends JView {
	function display($tpl = null) {
		$this->addToolBar();
		$model	= $this->getModel();
		/* load model data */
		$this->state		= $this->get('State');
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		
		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	protected function addToolBar()  {
		JToolBarHelper::title(JText::_('COM_DEVILIB_EMAILTEST'), 'generic.png');
	}
	/**
	 * Method to set up the document properties
	 * @return void
	 */
	protected function setDocument() {
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_DEVILIB_ADMINISTRATION'));
	}
}
