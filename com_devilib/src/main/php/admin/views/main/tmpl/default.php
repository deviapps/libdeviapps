<?php
/**
 * @author		Cyprian Sniegota
 * @package		devilib.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted Access');
JHtml::_('behavior.tooltip');
?>
<form action="<?php echo JRoute::_('index.php?option=com_devilib'); ?>" method="post" name="adminForm">
	<p>
		Devilib configuration component
	</p>
	<p>
		Created by HMail.pl, Licence: <a href="http://www.gnu.org/copyleft/gpl.html">GPLv3</a>
	</p>
	<p>
		Contact: <a href="mailto:cyprian@hmail.pl">cyprian@hmail.pl</a>
	</p>
	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>