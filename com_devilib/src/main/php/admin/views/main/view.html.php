<?php
/**
 * @author		Cyprian Sniegota
 * @package		devilib.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

/**
 * Main view
 */
class DevilibViewMain extends JView {
	function display($tpl = null) {
		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	protected function addToolBar()  {
		JToolBarHelper::title(JText::_('COM_DEVILIB_MAIN'), 'generic.png');
		JToolBarHelper::preferences('com_devilib');
	}
	/**
	 * Method to set up the document properties
	 * @return void
	 */
	protected function setDocument() {
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_DEVILIB_ADMINISTRATION'));
	}
}
