<?php
/**
 * @author		Cyprian Sniegota
 * @package		devilib.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');
jimport('devilib.email.devisendemailhelper');
class DevilibModelEmailtest extends JModel {
	public function testEmail() {
		echo "Sending error test: ";
		$ret = DeviSendEmailHelper::sendError("Test error message");
		echo ($ret === true)?"ok":"err";
		echo "<br />";
		echo "Sending sample test: ";
		$ret = DeviSendEmailHelper::sendEmail('studyfinish', 'cyprianno@gmail.com', array());
		echo ($ret === true)?"ok":"err";
		echo "<br />";
		return true;
	}
}
