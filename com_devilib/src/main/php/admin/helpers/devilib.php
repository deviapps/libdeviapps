<?php
/**
 * @author		Cyprian Sniegota
 * @package		devilib.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die;
class DevilibHelper {
	/**
	 * Configure the submenu linkbar.
	 */
	public static function addSubmenu($submenu) {
		JSubMenuHelper::addEntry(JText::_('COM_DEVILIB_SUBMENU_MAIN'), 'index.php?option=com_devilib', $submenu == 'main');
		JSubMenuHelper::addEntry(JText::_('COM_DEVILIB_SUBMENU_EMAILTEST'), 'index.php?option=com_devilib&view=emailtest', $submenu == 'sets');
		/*
		JSubMenuHelper::addEntry(JText::_('COM_DEVILIB_SUBMENU_ITEMS'), 'index.php?option=com_devilib&view=items', $submenu == 'items'); */
	}
}
