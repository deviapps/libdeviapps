<?php
/**
 * @author		Cyprian Sniegota
 * @package		devilib.library
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.database.table');
jimport('joomla.application.router');
//require_once(JPATH_SITE.DS.'includes'.DS.'router.php');

class DeviSendEmailHelper {

	public static $testEmail = null;
	public static $lastError = '';

	private static function replaceTags($str,$tagsArray) {
		foreach ($tagsArray as $key=>$value) {
			$str = str_replace('{'.$key.'}', $value, $str);
		}
		return $str;
	}

	private static function getDefaultSender() {
		$conf = JComponentHelper::getParams('com_devilib');
		return $conf->get('defaultfrom','root@localhost');
	}

	private static function getErrorEmail() {
		$conf = JComponentHelper::getParams('com_devilib');
		return $conf->get('erroremail','root@localhost');
	}

	/**
	 *
	 * @return array
	 */
	private static function getBCCEmail() {
		$conf = JComponentHelper::getParams('com_devilib');
		$bcc = $conf->get('bccemail','');
		if ($bcc == '') return '';
		$bccs = explode(",",$bcc);
		return $bccs;
	}

	private static function sendEmailEnabled()
	{
		$conf = JComponentHelper::getParams('com_devilib');
		$sendEnabled = $conf->get('send_enabled','1');
		return $sendEnabled == 1;
	}

	private static function replaceHref($body) {
		$base	= JURI::base(true).'/';
		$regex  = '#href="index.php\?([^"]*)#m';
		$body = preg_replace_callback($regex, array('DeviSendEmailHelper', 'route'), $body);
		return $body;
	}

	protected static function route(&$matches) {
		$original	= $matches[0];
		$url		= $matches[1];
		$url		= 'index.php?'.str_replace('&amp;','&',$url);
		$router = new JRouterSite(array('mode'=>JROUTER_MODE_SEF));
		$uri = $router->build($url);
		$Itemid = $uri->getVar('Itemid');
		if (!empty($Itemid)) {
			$uri = $router->build('index.php?Itemid='.$Itemid);
		}
		$newUrl = $uri->toString(array('path', 'query', 'fragment'));
		// SEF URL !
		$newUrl = str_replace('/administrator/', '', $newUrl);
		//and now the tidying, as Joomlas JRoute makes a cockup of the urls.
		$newUrl = str_replace('component/content/article/', '', $newUrl);
		// Replace spaces.
		$url = preg_replace('/\s/u', '%20', $newUrl);

		// Determine which scheme we want.
		$scheme	= 'https';

		// Make sure our URL path begins with a slash.
		if (!preg_match('#^/#', $url)) {
			$url = '/'.$url;
		}

		$currUri = JURI::getInstance();

		// Build the URL.
		$url = $scheme.'://'.$currUri->toString(array('host')).$url;

		$route		= $url;

		return 'href="'.$route;
	}

	public static function sendError($msg, $headers = array()) {
		$mailer = JFactory::getMailer();
		$mailer->isHTML(false);
		$mailer->Encoding = 'base64';

		$sender = array(
				self::getDefaultSender(),
				'Devi Error'
		);
		$mailer->setSender($sender);
		$mailer->addRecipient(self::getErrorEmail());
		$mailer->setSubject("Web error");
		$mailer->setBody($msg);
		foreach ($headers as $header) {
			$mailer->AddCustomHeader($header);
// 			$mailer->AddCustomHeader("X-Mailer-App: behavioapp");
// 			$mailer->AddCustomHeader("X-Marker: AB90785634YZ");
		}
		if (self::sendEmailEnabled()) {
			$send =& $mailer->Send();
		} else {
			$send = true;
		}
		return $send;
	}
	
	public static function prepareContent($code, $replaceArray, $lang = '*') {
		$code = strtolower($code);
		$ret = array('subject'=>'','body'=>'');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_bzstudy'.DS.'tables');
		$emailContent = JTable::getInstance('content');
		$contentCode = $code;
		if ($lang != '*') {
			$contentCode = $code."-".strtolower($lang);
		}
		$emailContent->load(array('alias'=>$contentCode));
		if (empty($emailContent->id)) {
			$emailContent->load(array('alias'=>$code));
		}
		if (empty($emailContent->id)) {
			self::sendError("Brak treści email o kodzie ".$code." ($lang) wysyłanej do ".$sendTo." dla danych: ".print_r($replaceArray,true));
			return false;
		}
		
		$subject = self::replaceTags($emailContent->title,$replaceArray);
		$body = self::replaceTags($emailContent->introtext,$replaceArray);
		$body = self::replaceHref($body);
		$body = '<html><head><style> ul { margin-top: 0px; margin-bottom:0px; } p { margin: 5px 0px 0px 0px; } li { margin-top:0px; margin-bottom:0px; }</style></head><body>'.$body.'</body></html>';
		$ret['subject'] = $subject;
		$ret['body'] = $body;
		$senderUser = JFactory::getUser($emailContent->created_by);
		$sender = array(
			$senderUser->email,
			$senderUser->name
		);
		$ret['sender'] = $sender;
		return $ret;
	}

	public static function sendEmail($code,$sendTo, $replaceArray, $attachments = null, $cc = null, $senderArr = null, $lang = "*", $headers = array()) {
		if (! $content = self::prepareContent($code, $replaceArray, $lang)) {
			return false;
		}
		$subject = $content['subject'];
		$body = $content['body'];
		if ($senderArr == null || empty($senderArr[0])) {
			$senderArr = $content['sender'];
		}
		return self::sendRawEmail($sendTo, $subject, $body, $attachments, $cc, $senderArr, $headers);
	}
	
	public static function sendRawEmail($sendTo, $subject, $body, $attachments = null, $cc = null, $senderArr = null, $headers = array()) {
		self::$lastError = '';
		$mailer = JFactory::getMailer();
		$mailer->isHTML(true);
		$mailer->Encoding = 'base64';
		$sender = $senderArr;
		$mailer->setSender($sender);
		if (!empty (self::$testEmail)) {
			$mailer->addRecipient(self::$testEmail);
		} else {
			$mailer->addRecipient($sendTo);
			if ($cc != null) {
				$mailer->AddCC($cc);
			}
		}
		$bccs = self::getBCCEmail();
		if (is_array($bccs)) {
			foreach ($bccs as $bcc) {
				$mailer->addBCC($bcc);
			}
		}
		
		$mailer->setSubject($subject);
		$mailer->setBody($body);
		foreach ($headers as $header) {
			$mailer->AddCustomHeader($header);
			// 			$mailer->AddCustomHeader("X-Mailer-App: behavioapp");
			// 			$mailer->AddCustomHeader("X-Marker: AB90785634YZ");
		}

		if ($attachments != null) {
			$mailer->addAttachment($attachments);
		}
		
		if (self::sendEmailEnabled()) {
			$send = $mailer->Send();
		} else {
			$send = true;
		}
		if ( $send !== true ) {
			self::$lastError = $mailer->ErrorInfo;
			self::sendError("Błąd wysyłania email o kodzie ".$code." wysyłanej do ".$sendTo." dla danych: ".print_r($replaceArray,true));
			return false;
		}
		return true;
	}
	
}
