<?php
/**
 * @author		Cyprian Sniegota
 * @package		devilib.library
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');

class DeviRouteHelper 
{
	
	public static function route($url,$xhtml=true)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__menu');
		$query->where('link='.$db->quote($url));
		$db->setQuery($query);
		$res = $db->loadResult();
		if ($res != null) {
			$rurl = JRoute::_('index.php?Itemid='.$res,$xhtml);
		} else {
			$rurl = JRoute::_($url,$xhtml);
		}
		return $rurl;
	}
}