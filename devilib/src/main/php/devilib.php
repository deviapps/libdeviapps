<?php
/**
 * @author		Cyprian Sniegota
 * @package		devilib.library
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}
if (!defined('DEVILIB_PATH')) {
        define('DEVILIB_PATH', dirname(__FILE__));
}

require_once dirname(__FILE__) . DS . 'loader.php';