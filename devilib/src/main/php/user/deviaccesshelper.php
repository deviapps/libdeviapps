<?php
/**
 * @author		Cyprian Sniegota
 * @package		devilib.library
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');

class DeviAccessHelper {

	static function getGroupByName($name) {
		$db = JFactory::getDbo();
		$query = "SELECT * FROM #__usergroups WHERE title=".$db->quote($name);
		$db->setQuery($query);
		$group = $db->loadObject();
		return $group;
	}

	static function checkUserInGroupByName($userId,$groupName) {
		$userGroups = JAccess::getGroupsByUser($userId);
		$group = self::getGroupByName($groupName);
		if ($group == null) return false;
		return in_array($group->id, $userGroups);
	}

}